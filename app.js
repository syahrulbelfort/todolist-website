const express = require('express');
const ejs = require('ejs');
const app = express();
const port = 3100;

app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));

let items = ['Buy a food', 'Drink some water', 'playing games'];
let workItems = [];

app.get('/', (req, res) => {
  const today = new Date();

  const options = {
    weekday: 'long',
    day: 'numeric',
    month: 'long',
  };

  const day = today.toLocaleDateString('en-US', options);

  res.render('list', { listTitle: day, newListItems: items });
});

app.post('/', (req, res) => {
  let item = req.body.newItem;
  console.log(req.body);
  if (req.body.list === 'Work') {
    workItems.push(item);
    res.redirect('/work');
  } else {
    items.push(item);
    res.redirect('/');
  }
});

app.get('/work', (req, res) => {
  res.render('list', { listTitle: 'Work list', newListItems: workItems });
});

app.post('/work', (req, res) => {
  const item = req.body.workItems;
  workItems.push(item);
  res.redirect('/work');
});

app.listen(port, () => {
  console.log('server is okay!');
});
